FROM varnish:stable

RUN apt-get update && apt-get install -y vim

COPY default.vcl /etc/varnish/
COPY docker-varnish-entrypoint /etc/varnish/

RUN chmod -R g=u /var/lib/varnish

USER 1001
