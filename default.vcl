vcl 4.0;

import std;

backend zope {
    .host = "giovazoom-plone";
    .port = "8080";
    .connect_timeout = 0.4s;
    .first_byte_timeout = 300s;
    .between_bytes_timeout = 60s;
}

backend volto {
    .host = "giovazoom-volto";
    .port = "3000";
    .connect_timeout = 0.4s;
    .first_byte_timeout = 300s;
    .between_bytes_timeout = 60s;
}

#acl purge {
#    "localhost";
#    "127.0.0.1";
#}

sub vcl_recv {
    set req.grace = 120s;
    set req.backend_hint = volto;

    if (req.url ~ "/giovazoom/..api..") {
        set req.backend_hint = zope;
        set req.url = regsub(req.url, "/giovazoom", "");
        set req.url = "/VirtualHostBase/http/" + req.http.host + "/Plone/VirtualHostRoot/_vh_giovazoom" + req.url;
        return(pass);
    }

    # Only deal with "normal" types
    if (req.method != "GET" &&
           req.method != "HEAD" &&
           req.method != "PUT" &&
           req.method != "POST" &&
           req.method != "TRACE" &&
           req.method != "OPTIONS" &&
           req.method != "DELETE") {
        /* Non-RFC2616 or CONNECT which is weird. */
        return(pipe);
    }

    if (req.method != "GET" && req.method != "HEAD") {
        # We only deal with GET and HEAD by default
        return(pass);
    }

    if (req.http.Expect) {
        return(pipe);
    }

    if (req.http.If-None-Match && !req.http.If-Modified-Since) {
        return(pass);
    }

    # unset req.http.X-Body-Len;
    #  if (req.method == "POST" && req.url ~ "/@querystring-search$") {
    #    std.log("Will cache POST for: " + req.http.host + req.url);
    #      std.cache_req_body(500KB);
    #     set req.http.X-Body-Len = bodyaccess.len_req_body();
    #      if (req.http.X-Body-Len == "-1") {
    #          return(synth(400, "The request body size exceeds the limit"));
    #      }
    #     # return (hash);
    #      set req.method = "GET";
    #  }

    if (req.url ~ "/@@download/file/") {
      return(pipe);
    }

    # call normalize_accept_encoding;
    # call annotate_request;
    # if (!req.http.X-Anonymous) {
    #     return(pass);
    # }
    return(pass);
    # return(hash);
}


sub vcl_backend_response {

    # se non c'è il cookie auth_token non ha senso fare un set-cookie per toglierlo
    # if (!bereq.http.cookie ~ "auth_token") {
    #   header.remove(beresp.http.Set-Cookie, "auth_token=;");
    # }

    # if (bereq.http.X-Body-Len) {
    #     set bereq.method = "POST";
    # }

    # The object is not cacheable
    if (beresp.http.Set-Cookie) {
        set beresp.http.X-Cacheable = "NO - Set Cookie";
        set beresp.ttl = 0s;
        set beresp.uncacheable = true;
    } elsif (beresp.http.Cache-Control ~ "private") {
        set beresp.http.X-Cacheable = "NO - Cache-Control=private";
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;
    } elsif (beresp.http.Surrogate-control ~ "no-store") {
        set beresp.http.X-Cacheable = "NO - Surrogate-control=no-store";
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;
    } elsif (!beresp.http.Surrogate-Control && beresp.http.Cache-Control ~ "no-cache|no-store") {
        set beresp.http.X-Cacheable = "NO - Cache-Control=no-cache|no-store";
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;
    } elsif (beresp.http.Vary == "*") {
        set beresp.http.X-Cacheable = "NO - Vary=*";
        set beresp.uncacheable = true;
        set beresp.ttl = 120s;

    # ttl handling
    } elsif (beresp.ttl < 0s) {
        set beresp.http.X-Cacheable = "NO - TTL < 0";
        set beresp.uncacheable = true;
    } elsif (beresp.ttl == 0s) {
        set beresp.http.X-Cacheable = "NO - TTL = 0";
        set beresp.uncacheable = true;

    # Varnish determined the object was cacheable
    } else {
        set beresp.http.X-Cacheable = "YES";
    }

    # Do not cache 5xx errors
    if (beresp.status >= 500 && beresp.status < 600) {
        unset beresp.http.Cache-Control;
        set beresp.http.X-Cache = "NOCACHE";
        set beresp.http.Cache-Control = "no-cache, max-age=0, must-revalidate";
        set beresp.ttl = 0s;
        set beresp.http.Pragma = "no-cache";
        set beresp.uncacheable = true;
        return(deliver);
    }

    # TODO this one is very plone specific and should be removed, not sure if its needed any more
    if (bereq.url ~ "(createObject|@@captcha)") {
        set beresp.uncacheable = true;
        return(deliver);
    }

    set beresp.grace = 600s;

    return (deliver);
}

sub vcl_hit {
    if (obj.ttl >= 0s) {
        // A pure unadultered hit, deliver it
        # normal hit
        return (deliver);
    }

    # We have no fresh fish. Lets look at the stale ones.
    if (std.healthy(req.backend_hint)) {
        # Backend is healthy. Limit age to 10s.
        if (obj.ttl + 10s > 0s) {
            set req.http.grace = "normal(limited)";
            return (deliver);
        } else {
            # No candidate for grace. Fetch a fresh object.
            return(miss);
        }
    } else {
        # backend is sick - use full grace
        // Object is in grace, deliver it
        // Automatically triggers a background fetch
        if (obj.ttl + obj.grace > 0s) {
            set req.http.grace = "full";
            return (deliver);
        } else {
            # no graced object.
            return (miss);
        }
    }

    if (req.method == "PURGE") {
        set req.method = "GET";
        set req.http.X-purger = "Purged";
        return(synth(200, "Purged. in hit " + req.url));
    }

    // fetch & deliver once we get the result
    return (miss); # Dead code, keep as a safeguard
}

sub vcl_miss {

    if (req.method == "PURGE") {
        set req.method = "GET";
        set req.http.X-purger = "Purged-possibly";
        return(synth(200, "Purged. in miss " + req.url));
    }

    // fetch & deliver once we get the result
    return (fetch);
}

sub vcl_deliver {
    set resp.http.grace = req.http.grace;
    if (obj.hits > 0) {
         set resp.http.X-Cache = "HIT";
    } else {
        set resp.http.X-Cache = "MISS";
    }
    /* Rewrite s-maxage to exclude from intermediary proxies
      (to cache *everywhere*, just use 'max-age' token in the response to avoid
      this override) */
    if (resp.http.Cache-Control ~ "s-maxage") {
        set resp.http.Cache-Control = regsub(resp.http.Cache-Control, "s-maxage=[0-9]+", "s-maxage=0");
    }
    /* Remove proxy-revalidate for intermediary proxies */
    if (resp.http.Cache-Control ~ ", proxy-revalidate") {
        set resp.http.Cache-Control = regsub(resp.http.Cache-Control, ", proxy-revalidate", "");
    }
}

sub vcl_hash {
    hash_data(req.http.X-RestApi);
    # To cache POST and PUT requests
    # if (req.http.X-Body-Len) {
    #     bodyaccess.hash_req_body();
    # } else {
    #     hash_data("");
    # }
}


##########################
#  Helper Subroutines
##########################

# Optimize the Accept-Encoding variant caching
#sub normalize_accept_encoding {
#    if (req.http.Accept-Encoding) {
#        if (req.url ~ "\.(jpe?g|png|gif|swf|pdf|gz|tgz|bz2|tbz|zip)$" || req.url ~ "/image_[^/]*$") {
#            remove req.http.Accept-Encoding;
#        } elsif (req.http.Accept-Encoding ~ "gzip") {
#            set req.http.Accept-Encoding = "gzip";
#        } else {
#            remove req.http.Accept-Encoding;
#        }
#    }
#}
#
## Keep auth/anon variants apart if "Vary: X-Anonymous" is in the response
#sub annotate_request {
#    if (!(req.http.Authorization || req.http.cookie ~ "(^|.*; )__ac=")) {
#        set req.http.X-Anonymous = "True";
#    }
#}
#
## The varnish response should always declare itself to be fresh
#sub rewrite_age {
#    if (resp.http.Age) {
#        set resp.http.X-Varnish-Age = resp.http.Age;
#        set resp.http.Age = "0";
#    }
#}
#
## Rewrite s-maxage to exclude from intermediary proxies
## (to cache *everywhere*, just use 'max-age' token in the response to avoid this override)
#sub rewrite_s_maxage {
#    if (beresp.http.Cache-Control ~ "s-maxage") {
#        set beresp.http.Cache-Control = regsub(beresp.http.Cache-Control, "s-maxage=[0-9]+", "s-maxage=0");
#    }
#}


